//
//  AppDelegate.swift
//  Kext Updater
//
//  Created by Sascha Lamprecht on 04.11.2019.
//  Copyright © 2019 Sascha Lamprecht. All rights reserved.
//

import Cocoa
import LoginServiceKit
import LetsMove

class AppDelegate: NSObject, NSApplicationDelegate {
        
    //strong reference to retain the status bar item object
	var statusItem: NSStatusItem?
    
    @IBOutlet weak var appMenu: NSMenu!

    @objc func displayMenu() {
        
        guard let button = statusItem?.button else { return }
        let x = button.frame.origin.x
        let y = button.frame.origin.y - 5
        let location = button.superview!.convert(NSMakePoint(x, y), to: nil)
        let w = button.window!
        let event = NSEvent.mouseEvent(with: .leftMouseDown,
                                       location: location,
                                       modifierFlags: NSEvent.ModifierFlags(rawValue: 0),
                                       timestamp: 0,
                                       windowNumber: w.windowNumber,
                                       context: w.graphicsContext,
                                       eventNumber: 0,
                                       clickCount: 1,
                                       pressure: 0)!
        NSMenu.popUpContextMenu(appMenu, with: event, for: button)
    }
	
	func applicationDidFinishLaunching(_ aNotification: Notification) {

        statusItem = NSStatusBar.system.statusItem(withLength: -1)
        
        guard let button = statusItem?.button else {
            print("Status bar item failed. Try removing some menu bar item.")
            NSApp.terminate(nil)
            return
        }
       
        enum InterfaceStyle : String {
           case Dark, Light

           init() {
              let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
              self = InterfaceStyle(rawValue: type)!
            }
        }

        let currentStyle = InterfaceStyle()
        
        if currentStyle.rawValue == "Light"{
            button.image = NSImage(named: "logo_28")
        } else{
            button.image = NSImage(named: "logo_dark_28")
        }
        
        button.target = self
        button.action = #selector(self.detect_mouse_button(sender:))
        button.sendAction(on: [.leftMouseUp, .rightMouseUp])
        
        PFMoveToApplicationsFolderIfNecessary()
        
        let loginitem = UserDefaults.standard.bool(forKey: "Loginitem")
               if loginitem == true {
               LoginServiceKit.addLoginItems(at: Bundle.main.bundlePath)
               } else {
                   let isExistLoginItem = LoginServiceKit.isExistLoginItems(at: Bundle.main.bundlePath)
                       if isExistLoginItem == true {
                           LoginServiceKit.removeLoginItems(at: Bundle.main.bundlePath)
                       }
               }
        
        let browser = UserDefaults.standard.string(forKey: "Browser")
        if browser == nil{
            UserDefaults.standard.set("0", forKey: "Browser")
        }
    }

    @objc func detect_mouse_button(sender: NSStatusItem) {

        let event = NSApp.currentEvent!

        if event.type == NSEvent.EventType.rightMouseUp{
            self.displayMenu()
        } else {
            
            let browser = UserDefaults.standard.string(forKey: "Browser")
            
            if browser == "0" {
                UserDefaults.standard.set("", forKey: "BrowserID")
            } else if browser == "1" {
                UserDefaults.standard.set("com.google.Chrome", forKey: "BrowserID")
            }else if browser == "2" {
                UserDefaults.standard.set("org.chromium.Chromium", forKey: "BrowserID")
            }else if browser == "3" {
                UserDefaults.standard.set("org.mozilla.firefox", forKey: "BrowserID")
            }else if browser == "4" {
                UserDefaults.standard.set("com.operasoftware.Opera", forKey: "BrowserID")
            }else if browser == "5" {
                UserDefaults.standard.set("com.apple.Safari", forKey: "BrowserID")
            }
            
            let appId = UserDefaults.standard.string(forKey: "BrowserID")
            
            let url = URL(string:"https://www.hackintosh-forum.de")!
            NSWorkspace.shared.open([url],
                                    withAppBundleIdentifier: appId,
                                    options: [],
                                    additionalEventParamDescriptor: nil,
                                    launchIdentifiers: nil)
        }
    }
    
    @IBAction func quit_menubar(_ sender: Any) {
                NSApplication.shared.terminate(self)
            }
}


